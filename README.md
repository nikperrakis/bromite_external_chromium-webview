# Bromite Webview

[Bromite](https://www.bromite.org/) is Chromium plus ad blocking and privacy enhancements. The main goal is to provide a no-clutter browsing experience without privacy-invasive features and with the addition of a fast ad-blocking engine. Minimal UI changes are applied to help curbing the idea of “browser as an advertisement platform”.


Current Bromite stable version: 74.0.3729.141

-----

Repository initially forked from [LineageOS Webview repository](https://github.com/LineageOS/android_external_chromium-webview)

Building the Chromium-based WebView in AOSP is no longer supported. WebView can
now be built entirely from the Chromium source code.

General instructions for building WebView from Chromium:
https://www.chromium.org/developers/how-tos/build-instructions-android-webview


For questions about building WebView, please see
https://groups.google.com/a/chromium.org/forum/#!forum/android-webview-dev
